package com.mtsb.finalproject.scheduler;

import com.mtsb.finalproject.entity.Loan;
import com.mtsb.finalproject.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@Component
@RequiredArgsConstructor
public class LoanOrderScheduler {

    private final OrderService orderService;

    @Scheduled(fixedRate = 120000)
    public void processLoanOrders() {
        List<Loan> loanOrders = orderService.findAllByStatus("IN_PROGRESS");

        Random random = new Random();
        for (Loan loanOrder : loanOrders) {
            boolean approved = random.nextBoolean();
            String newStatus = approved ? "APPROVED" : "REFUSED";
            loanOrder.setStatus(newStatus);
            Timestamp timeUpdate = new Timestamp(System.currentTimeMillis());
            loanOrder.setTimeUpdate(timeUpdate);
            orderService.update(loanOrder);
        }
    }
}
