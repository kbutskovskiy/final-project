package com.mtsb.finalproject.controller;


import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseTariffs;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

@Controller
@RequiredArgsConstructor
public class TariffController {

    @GetMapping("/loan-service/getTariffs-view")
    public String getTariff(Model model) {
        RestTemplate restTemplate = new RestTemplate();
        String jsonUrl = "http://localhost:8080/loan-service/getTariffs";
        ResponseEntity<Response<SuccessResponseTariffs>> response = restTemplate.exchange(jsonUrl, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {});
        if (response.getBody() == null) {
            model.addAttribute("error", "Метод долго работает");
            return "loan-error";
        }
        Response<SuccessResponseTariffs> tariffsResponse = response.getBody();
        model.addAttribute("tariffs", tariffsResponse.getData().getTariffs());
        return "tariff";
    }
}
