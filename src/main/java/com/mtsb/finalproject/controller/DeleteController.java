package com.mtsb.finalproject.controller;

import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.service.DeleteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Slf4j
@Controller
@RequiredArgsConstructor
public class DeleteController {

    private final DeleteService deleteService;

    @PostMapping("/loan-service/deleteOrder-view")
    public String deleteOrder(@RequestParam("tariffType") String tariffType, @RequestParam("username") String username){
        log.info("START deleting");
        Long userId = deleteService.getUserIdByUsername(username);
        Optional<String> orderId = deleteService.getOrderIdByTariffTypeAndUserId(tariffType, userId);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/loan-service/deleteOrder";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("userId", userId);
        jsonRequest.put("orderId", orderId.get());

        HttpEntity<String> entity = new HttpEntity<>(jsonRequest.toString(), headers);

        restTemplate.exchange(url, HttpMethod.DELETE, entity, ApiResponse.class);
        log.info("STOP deleting");
        return "delete-success";
    }
}
