package com.mtsb.finalproject.controller;

import com.mtsb.finalproject.security.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/loan-service")
public class RegisterController {

    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/register")
    public String registerUser(@RequestParam("username") String username, @RequestParam("password") String password) throws Exception {
        JdbcUserDetailsManager jdbcUserDetailsManager = securityConfig.jdbcUserDetailsManager();
        if (!jdbcUserDetailsManager.userExists(username)) {
            jdbcUserDetailsManager.createUser(User.builder()
                    .username(username)
                    .password(passwordEncoder.encode(password))
                    .roles("USER")
                    .build());
            return "redirect:/loan-service/getTariffs-view";
        } else {
            return "redirect:/loan-service/registration?error";
        }
    }

}