package com.mtsb.finalproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/loan-service")
    public String getMainPage(){
        return "index";
    }
}
