package com.mtsb.finalproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/loan-service")
public class RegistrationController {

    @GetMapping("/registration")
    public String showRegistrationForm() {
        return "registration";
    }
}