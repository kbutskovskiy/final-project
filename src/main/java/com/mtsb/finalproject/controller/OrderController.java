package com.mtsb.finalproject.controller;

import com.mtsb.finalproject.entity.Tariff;
import com.mtsb.finalproject.entity.User;
import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.service.OrderService;
import com.mtsb.finalproject.service.TariffService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Controller
@RequiredArgsConstructor
@Slf4j
public class OrderController {

    private final OrderService orderService;
    private final TariffService tariffService;

    @PostMapping("/loan-service/order/{tariffType}-view")
    public String processing(@PathVariable String tariffType, Model model){
        log.info("START loan order processing");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        log.info("username: {}", userDetails.getUsername());
        Optional<Long> tariffId = tariffService.getTariffIdByType(tariffType);
        Optional<User> user = orderService.getUser(userDetails.getUsername());
        if (user.isEmpty()){
            return "wrong-user";
        } else {
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/loan-service/order";

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("userId", user.get().getId());
            jsonRequest.put("tariffId", tariffId.get());

            HttpEntity<String> entity = new HttpEntity<>(jsonRequest.toString(), headers);

            ResponseEntity<?> response = restTemplate.exchange(url, HttpMethod.POST, entity, ApiResponse.class);

            if (response.getBody() == null) {
                model.addAttribute("error", "Метод работает долго");
                return "loan-error";
            }

            log.info("STOP loan order processing");

            return "loan-success";
        }
    }
}
