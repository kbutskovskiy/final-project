package com.mtsb.finalproject.controller;

import com.mtsb.finalproject.service.StatusService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
@RequiredArgsConstructor
public class StatusController {

    private final StatusService statusService;

    @PostMapping("/loan-service/getStatusOrder-view")
    public String getStatusPost(Model model){
        log.info("START getStatus processing");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        log.info("STOP getStatus processing");
        String page = statusService.statusProcessing(userDetails.getUsername(), model);
        if (page == null) {
            model.addAttribute("error", "Метод долго работает");
            return "loan-error";
        }
        return page;
    }
}
