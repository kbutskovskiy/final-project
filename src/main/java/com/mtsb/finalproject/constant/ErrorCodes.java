package com.mtsb.finalproject.constant;

public enum ErrorCodes {
    TARIFF_NOT_FOUND,
    LOAN_CONSIDERATION,
    LOAN_ALREADY_APPROVED,
    TRY_LATER,
    UNEXPECTED_ERROR,
    ORDER_NOT_FOUND,
    ORDER_IMPOSSIBLE_TO_DELETE,
    USER_NOT_FOUND
}
