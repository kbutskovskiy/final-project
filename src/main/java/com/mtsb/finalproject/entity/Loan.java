package com.mtsb.finalproject.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "loan_order")
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "tariff_id")
    private Long tariffId;

    @Column(name = "credit_rating")
    private Double credit_rating;

    @Column(name = "status")
    private String status;

    @Column(name = "time_insert")
    private Timestamp timeInsert;

    @Column(name = "time_update")
    private Timestamp timeUpdate;
}
