package com.mtsb.finalproject.service;

import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseTariffs;

import java.util.Optional;

public interface TariffService {
    Optional<Long> getTariffIdByType(String tariffType);
    Response<SuccessResponseTariffs> getTariffs();

    Response<SuccessResponseTariffs> getTariffsFallback();
}
