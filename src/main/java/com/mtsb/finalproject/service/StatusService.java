package com.mtsb.finalproject.service;

import com.mtsb.finalproject.response.ApiResponse;
import org.springframework.ui.Model;

import java.util.UUID;

public interface StatusService {
    String statusProcessing(String username, Model model);
    String statusProcessingFallback(String username, Model model);

    ApiResponse getStatus(UUID orderId);
    ApiResponse getStatusFallback(UUID orderId);
}
