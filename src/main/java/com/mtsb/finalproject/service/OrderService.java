package com.mtsb.finalproject.service;

import com.mtsb.finalproject.entity.Loan;
import com.mtsb.finalproject.entity.User;
import com.mtsb.finalproject.response.ApiResponse;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    Optional<User> getUser(String username);

    ApiResponse tariffRestProcessing(Long userId, Long tariffId);

    ApiResponse orderFallback(Long userId, Long tariffId);

    void update(Loan loan);

    List<Loan> findAllByStatus(String status);
}
