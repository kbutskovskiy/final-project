package com.mtsb.finalproject.service;

import java.util.Optional;

public interface DeleteService {

    void deleteFromLoan(Long userId, String orderId);

    Optional<String> getOrderIdByTariffTypeAndUserId(String tariffType, Long userId);

    Long getUserIdByUsername(String username);
}
