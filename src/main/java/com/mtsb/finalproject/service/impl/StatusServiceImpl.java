package com.mtsb.finalproject.service.impl;

import com.mtsb.finalproject.entity.Loan;
import com.mtsb.finalproject.entity.User;
import com.mtsb.finalproject.exception.OrderImpossibleToDeleteException;
import com.mtsb.finalproject.exception.OrderNotFoundException;
import com.mtsb.finalproject.repository.LoanRepository;
import com.mtsb.finalproject.repository.TariffRepository;
import com.mtsb.finalproject.repository.UserRepository;
import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseStatus;
import com.mtsb.finalproject.service.StatusService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.*;

@Service
@RequiredArgsConstructor
public class StatusServiceImpl implements StatusService {

    private final UserRepository userRepository;
    private final TariffRepository tariffRepository;
    private final LoanRepository loanRepository;

    @Override
    @HystrixCommand(fallbackMethod = "statusProcessingFallback",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            })
    public String statusProcessing(String username, Model model) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            return "wrong-user";
        }

        List<Loan> loanList = loanRepository.findListLoanByUserId(user.get().getId());
        if (loanList.size() == 0) {
            return "loan-list-empty";
        }

        Map<String, String> statuses = new HashMap<>();

        for (Loan loan : loanList) {
            Optional<String> type = tariffRepository.getTypeByTariffId(loan.getTariffId());
            type.ifPresent(s -> statuses.put(s, loan.getStatus()));
        }
        model.addAttribute("statuses", statuses);
        model.addAttribute("username", username);
        return "loan-status";
    }

    @Override
    public String statusProcessingFallback(String username, Model model) {
        return null;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getStatusFallback",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            },
            ignoreExceptions = OrderNotFoundException.class)
    public ApiResponse getStatus(UUID orderId) {
        Optional<String> status = loanRepository.findStatusByOrderId(orderId.toString());
        if (status.isEmpty()) {
            throw new OrderNotFoundException("Заявка не найдена");
        }
        SuccessResponseStatus successResponseStatus = new SuccessResponseStatus(status.get());
        return new Response<>(successResponseStatus);
    }

    @Override
    public ApiResponse getStatusFallback(UUID orderId) {
        return null;
    }
}
