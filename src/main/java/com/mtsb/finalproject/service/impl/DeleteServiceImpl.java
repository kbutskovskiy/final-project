package com.mtsb.finalproject.service.impl;

import com.mtsb.finalproject.exception.OrderImpossibleToDeleteException;
import com.mtsb.finalproject.exception.OrderNotFoundException;
import com.mtsb.finalproject.repository.LoanRepository;
import com.mtsb.finalproject.repository.TariffRepository;
import com.mtsb.finalproject.repository.UserRepository;
import com.mtsb.finalproject.service.DeleteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DeleteServiceImpl implements DeleteService {

    private final LoanRepository loanRepository;
    private final UserRepository userRepository;
    private final TariffRepository tariffRepository;


    @Override
    public void deleteFromLoan(Long userId, String orderId) {
        Optional<String> status = loanRepository.findStatusByOrderId(orderId);
        if (status.isEmpty()) {
            throw new OrderNotFoundException("Заявка не найдена");
        }
        if (!Objects.equals("IN_PROGRESS", status.get())) {
            throw new OrderImpossibleToDeleteException("Невозможно удалить заявку");
        }
        loanRepository.deleteLoan(userId, orderId);
    }

    @Override
    public Optional<String> getOrderIdByTariffTypeAndUserId(String tariffType, Long userId) {
        Optional<Long> tariffId = tariffRepository.getTariffIdByType(tariffType);
        return loanRepository.getOrderIdByTariffIdAndUserId(tariffId.get(), userId);
    }

    @Override
    public Long getUserIdByUsername(String username) {
        return userRepository.findByUsername(username).get().getId();
    }
}
