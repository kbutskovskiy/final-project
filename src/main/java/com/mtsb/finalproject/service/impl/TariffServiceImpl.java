package com.mtsb.finalproject.service.impl;

import com.mtsb.finalproject.entity.Tariff;
import com.mtsb.finalproject.exception.TariffNotFoundException;
import com.mtsb.finalproject.repository.TariffRepository;
import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseTariffs;
import com.mtsb.finalproject.service.TariffService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TariffServiceImpl implements TariffService {

    private final TariffRepository tariffRepository;

    @Override
    public Optional<Long> getTariffIdByType(String tariffType) {
        return tariffRepository.getTariffIdByType(tariffType);
    }

    @Override
    @HystrixCommand(fallbackMethod = "getTariffsFallback",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            })
    public Response<SuccessResponseTariffs> getTariffs(){
        List<Tariff> tariffs = tariffRepository.findAll();
        SuccessResponseTariffs successResponse = new SuccessResponseTariffs(tariffs);
        return new Response<>(successResponse);
    }

    @Override
    public Response<SuccessResponseTariffs> getTariffsFallback() {
        return null;
    }


}
