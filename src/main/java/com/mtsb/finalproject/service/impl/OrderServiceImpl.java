package com.mtsb.finalproject.service.impl;

import com.mtsb.finalproject.entity.Loan;
import com.mtsb.finalproject.entity.Tariff;
import com.mtsb.finalproject.entity.User;
import com.mtsb.finalproject.exception.*;
import com.mtsb.finalproject.repository.LoanRepository;
import com.mtsb.finalproject.repository.TariffRepository;
import com.mtsb.finalproject.repository.UserRepository;
import com.mtsb.finalproject.response.*;
import com.mtsb.finalproject.service.OrderService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final UserRepository userRepository;
    private final LoanRepository loanRepository;
    private final TariffRepository tariffRepository;

    @Override
    public Optional<User> getUser(String username) {
        return userRepository.findByUsername(username);
    }


    @Override
    @HystrixCommand(fallbackMethod = "orderFallback",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            },
            ignoreExceptions = {LoanConsiderationException.class, LoanAlreadyApprovedException.class,
                    TryLaterException.class, UnexpectedException.class, TariffNotFoundException.class,
            UserNotFoundException.class})
    public ApiResponse tariffRestProcessing(Long userId, Long tariffId) {
        Optional<Tariff> tariff = tariffRepository.getTariffById(tariffId);
        Optional<User> user = userRepository.findByUserId(userId);
        if (tariff.isEmpty()) {
            throw new TariffNotFoundException("Тариф не найден");
        }
        if (user.isEmpty()) {
            throw new UserNotFoundException("Пользователь не найден");
        }
        Loan loan = loanRepository.findLoanByTariffAndUserId(tariffId, userId);

        if (loan != null) {
            switch (loan.getStatus()) {
                case "IN_PROGRESS" -> throw new LoanConsiderationException("Заявка ожидает рассмотрения");

                case "APPROVED" -> throw new LoanAlreadyApprovedException("Заявка уже подтверждена");

                case "REFUSED" -> {
                    Duration duration = Duration.between(LocalDateTime.now(), loan.getTimeUpdate().toLocalDateTime());
                    if (duration.getSeconds() < 120) {
                        throw new TryLaterException("Попытайтесь сделать заявку позже");
                    } else {
                        throw new UnexpectedException("Попытайтесь сделать заявку позже");
                    }
                }
            }
        }
        String orderId = loanRepository.saveLoan(tariffId, userId);
        SuccessResponseOrder successResponseOrder = new SuccessResponseOrder(orderId);
        return new Response<>(successResponseOrder);
    }

    @Override
    public ApiResponse orderFallback(Long userId, Long tariffId) {
        return null;
    }

    @Override
    public void update(Loan loan) {
        loanRepository.updateLoan(loan);
    }

    @Override
    public List<Loan> findAllByStatus(String status) {
        return loanRepository.findListLoanByStatus(status);
    }
}
