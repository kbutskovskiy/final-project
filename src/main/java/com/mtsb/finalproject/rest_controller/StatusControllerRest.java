package com.mtsb.finalproject.rest_controller;

import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.service.StatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class StatusControllerRest {

    private final StatusService statusService;

    @GetMapping("/loan-service/getStatusOrder")
    public ResponseEntity<ApiResponse> getStatusOrder(@RequestParam("orderId")UUID orderId){
        ApiResponse apiResponse = statusService.getStatus(orderId);
        if (apiResponse == null) {
            return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
    }
}
