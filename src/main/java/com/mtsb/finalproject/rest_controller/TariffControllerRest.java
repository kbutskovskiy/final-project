package com.mtsb.finalproject.rest_controller;

import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseTariffs;
import com.mtsb.finalproject.service.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TariffControllerRest {

    private final TariffService tariffService;

    @GetMapping("/loan-service/getTariffs")
    public Response<SuccessResponseTariffs> getTariffs(){
        return tariffService.getTariffs();
    }
}
