package com.mtsb.finalproject.rest_controller;

import com.mtsb.finalproject.dto.OrderRestDTO;
import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OrderControllerRest {

    private final OrderService orderService;

    @PostMapping("/loan-service/order")
    public ResponseEntity<ApiResponse> getOrder(@RequestBody OrderRestDTO orderRestDTO) {
        ApiResponse apiResponse = orderService.tariffRestProcessing(orderRestDTO.getUserId(), orderRestDTO.getTariffId());
        if (apiResponse == null) {
            return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
    }
}
