package com.mtsb.finalproject.rest_controller;

import com.mtsb.finalproject.dto.OrderDeleteRestDTO;
import com.mtsb.finalproject.service.DeleteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OrderDeleteControllerRest {

    private final DeleteService deleteService;

    @DeleteMapping("/loan-service/deleteOrder")
    public void deleteOrder(@RequestBody OrderDeleteRestDTO orderDTO) {
        deleteService.deleteFromLoan(orderDTO.getUserId(), orderDTO.getOrderId().toString());
    }
}
