package com.mtsb.finalproject.response;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SuccessResponseStatus {
    private String orderStatus;
}


