package com.mtsb.finalproject.response;


import com.mtsb.finalproject.entity.Tariff;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuccessResponseTariffs {

    private List<Tariff> tariffs;
}
