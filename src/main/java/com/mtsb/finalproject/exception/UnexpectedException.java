package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class UnexpectedException extends CustomException{
    public UnexpectedException(String message){
        super(ErrorCodes.UNEXPECTED_ERROR, message);
    }
}
