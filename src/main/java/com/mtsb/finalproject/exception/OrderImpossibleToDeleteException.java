package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class OrderImpossibleToDeleteException extends CustomException{
    public OrderImpossibleToDeleteException(String message){
        super(ErrorCodes.ORDER_IMPOSSIBLE_TO_DELETE, message);
    }
}
