package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class LoanConsiderationException extends CustomException{
    public LoanConsiderationException(String message) {
        super(ErrorCodes.LOAN_CONSIDERATION, message);
    }
}
