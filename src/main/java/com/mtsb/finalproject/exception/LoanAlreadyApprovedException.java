package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class LoanAlreadyApprovedException extends CustomException{
    public LoanAlreadyApprovedException(String message) {
        super(ErrorCodes.LOAN_ALREADY_APPROVED, message);
    }
}
