package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class TryLaterException extends CustomException{
    public TryLaterException(String message) {
        super(ErrorCodes.TRY_LATER, message);
    }
}
