package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class OrderNotFoundException extends CustomException{

    public OrderNotFoundException(String message){
        super(ErrorCodes.ORDER_NOT_FOUND, message);
    }
}
