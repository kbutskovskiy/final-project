package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class UserNotFoundException extends CustomException{
    public UserNotFoundException(String message){
        super(ErrorCodes.USER_NOT_FOUND, message);
    }
}
