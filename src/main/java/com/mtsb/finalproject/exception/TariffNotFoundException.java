package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;

public class TariffNotFoundException extends CustomException{
    public TariffNotFoundException(String message){
        super(ErrorCodes.TARIFF_NOT_FOUND, message);
    }
}
