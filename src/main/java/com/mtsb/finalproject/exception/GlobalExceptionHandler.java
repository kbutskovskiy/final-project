package com.mtsb.finalproject.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.response.Error;
import com.mtsb.finalproject.response.ErrorResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final ObjectMapper objectMapper;

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ApiResponse> handleCustomException(CustomException exception){
        ErrorResponse errorResponse = new ErrorResponse(exception.getErrorCode().name(), exception.getMessage());
        Error error = new Error(errorResponse);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(HttpClientErrorException.BadRequest.class)
    public String handleLoanConsiderationException(HttpClientErrorException.BadRequest exception, Model model){
        String responseBody = exception.getResponseBodyAsString();
        log.info("Exception: {}", responseBody);
        try {
            Error errorResponse = objectMapper.readValue(responseBody, Error.class);
            String errorCode = errorResponse.getError().getCode();
            log.info("Code error: {}", errorCode);
            model.addAttribute("error", errorResponse.getError().getMessage());
        } catch (
                JsonProcessingException e) {
            log.error("Ошибка при десериализации JSON ответа", e);
            model.addAttribute("error", e.getMessage());
        }
        return "loan-error";
    }
}
