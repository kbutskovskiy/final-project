package com.mtsb.finalproject.exception;

import com.mtsb.finalproject.constant.ErrorCodes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomException extends RuntimeException{
    private ErrorCodes errorCode;

    public CustomException(ErrorCodes errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
