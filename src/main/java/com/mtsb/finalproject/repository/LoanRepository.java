package com.mtsb.finalproject.repository;

import com.mtsb.finalproject.entity.Loan;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LoanRepository {

    Loan findLoanByTariffAndUserId(Long tariffId, Long userId);

    Optional<String> getOrderIdByTariffIdAndUserId(Long tariffId, Long userId);

    String saveLoan(Long tariffId, Long userId);

    void deleteLoan(Long userId, String orderId);

    List<Loan> findListLoanByUserId(Long userId);

    void updateLoan(Loan loan);

    List<Loan> findListLoanByStatus(String status);

    Optional<String> findStatusByOrderId(String orderId);
}
