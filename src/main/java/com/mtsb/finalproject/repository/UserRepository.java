package com.mtsb.finalproject.repository;

import com.mtsb.finalproject.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository {
    Optional<User> findByUsername(String username);
    Optional<User> findByUserId(Long userId);
}
