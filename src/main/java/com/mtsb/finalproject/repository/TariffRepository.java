package com.mtsb.finalproject.repository;

import com.mtsb.finalproject.entity.Tariff;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TariffRepository {
    List<Tariff> findAll();
    Optional<Tariff> getTariffById(Long tariffId);
    Optional<String> getTypeByTariffId(Long tariffId);
    Optional<Long> getTariffIdByType(String tariffType);
}
