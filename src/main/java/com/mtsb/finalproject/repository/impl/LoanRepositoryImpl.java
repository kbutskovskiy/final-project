package com.mtsb.finalproject.repository.impl;

import com.mtsb.finalproject.entity.Loan;
import com.mtsb.finalproject.repository.LoanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class LoanRepositoryImpl implements LoanRepository {

    private final JdbcTemplate jdbcTemplate;


    @Override
    public Loan findLoanByTariffAndUserId(Long tariffId, Long userId) {
        List<Loan> loanList = jdbcTemplate.query("SELECT * FROM loan_order WHERE tariff_id=? AND user_id=?",
                this::mapRowToLoan, tariffId, userId);
        if (loanList.isEmpty()) {
            return null;
        }
        return loanList.get(0);
    }

    @Override
    public Optional<String> getOrderIdByTariffIdAndUserId(Long tariffId, Long userId) {
        try {
            String orderId = jdbcTemplate.queryForObject(
                    "SELECT order_id FROM loan_order WHERE tariff_id=? AND user_id=?",
                    new Object[]{tariffId, userId},
                    String.class
            );
            return Optional.ofNullable(orderId);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }


    @Override
    public String saveLoan(Long tariffId, Long userId){
        Random random = new Random();
        String orderId = UUID.randomUUID().toString();
        double creditRating = random.nextDouble() * 0.80 + 0.10;
        creditRating = Math.round(creditRating * 100.0) / 100.0;
        Timestamp timeInsert = new Timestamp(System.currentTimeMillis());
        jdbcTemplate.update(
                "INSERT INTO loan_order (order_id, user_id, tariff_id, credit_rating, status, time_insert," +
                        " time_update) values(?, ?, ?, ?, ?, ?, ?)",
                orderId,
                userId,
                tariffId,
                creditRating,
                "IN_PROGRESS",
                timeInsert,
                timeInsert);
        return orderId;
    }

    @Override
    public void deleteLoan(Long userId, String orderId) {
        String sqlQuery = "DELETE FROM loan_order WHERE user_id=? AND order_id=?";
        jdbcTemplate.update(sqlQuery, userId, orderId);
    }

    @Override
    public List<Loan> findListLoanByUserId(Long userId) {
        return jdbcTemplate.query("SELECT * FROM loan_order WHERE user_id=?",
                this::mapRowToLoan, userId);
    }


    @Override
    public void updateLoan(Loan loan) {
        String sql = "UPDATE loan_order SET status=?, time_update=? WHERE user_id=? AND tariff_id=?";
        jdbcTemplate.update(sql, loan.getStatus(), loan.getTimeUpdate(), loan.getUserId(), loan.getTariffId());
    }

    @Override
    public List<Loan> findListLoanByStatus(String status) {
        return jdbcTemplate.query("SELECT * FROM loan_order WHERE status=?",
                this::mapRowToLoan, status);
    }

    @Override
    public Optional<String> findStatusByOrderId(String orderId) {
        try {
            String status = jdbcTemplate.queryForObject(
                    "SELECT status FROM loan_order WHERE order_id=?",
                    new Object[]{orderId},
                    String.class
            );
            return Optional.ofNullable(status);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    private Loan mapRowToLoan(ResultSet row, int rowNum)
            throws SQLException {
        return new Loan(row.getLong("id"), row.getString("order_id"),
                row.getLong("user_id"), row.getLong("tariff_id"),
                row.getDouble("credit_rating"), row.getString("status"),
                row.getTimestamp("time_insert"), row.getTimestamp("time_update"));
    }
}
