package com.mtsb.finalproject.repository.impl;

import com.mtsb.finalproject.entity.User;
import com.mtsb.finalproject.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public Optional<User> findByUsername(String username) {
        List<User> users =  jdbcTemplate.query("SELECT * FROM usr WHERE username=?",
                this::mapRowToUser, username);
        if (users.size() == 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(users.get(0));
    }

    @Override
    public Optional<User> findByUserId(Long userId) {
        List<User> users =  jdbcTemplate.query("SELECT * FROM usr WHERE id=?",
                this::mapRowToUser, userId);
        if (users.size() == 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(users.get(0));
    }


    private User mapRowToUser(ResultSet row, int rowNum)
            throws SQLException {
        return new User(row.getLong("id"), row.getString("username"), row.getString("password"),
                row.getBoolean("enabled"), row.getBoolean("account_non_expired"), row.getBoolean("credentials_non_expired"),
                row.getBoolean("account_non_locked"));
    }
}
