package com.mtsb.finalproject.repository.impl;

import com.mtsb.finalproject.entity.Tariff;
import com.mtsb.finalproject.repository.TariffRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TariffRepositoryImpl implements TariffRepository {

    private final JdbcTemplate jdbcTemplate;
    @Override
    public List<Tariff> findAll() {
        List<Tariff> tariffs =  jdbcTemplate.query("SELECT id, type, interest_rate from tariff",
                this::mapRowToTariff);
        return new ArrayList<>(tariffs);
    }

    @Override
    public Optional<Tariff> getTariffById(Long tariffId) {
        List<Tariff> tariffs =  jdbcTemplate.query("SELECT id, type, interest_rate from tariff WHERE id=?",
                this::mapRowToTariff, tariffId);
        if (tariffs.isEmpty()){
            return Optional.empty();
        }
        return Optional.ofNullable(tariffs.get(0));
    }

    @Override
    public Optional<String> getTypeByTariffId(Long tariffId) {
        try {
            String type = jdbcTemplate.queryForObject(
                    "SELECT type FROM tariff WHERE id=?",
                    new Object[]{tariffId},
                    String.class
            );
            return Optional.ofNullable(type);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Long> getTariffIdByType(String tariffType) {
        try {
            Long id = jdbcTemplate.queryForObject(
                    "SELECT id FROM tariff WHERE type=?",
                    new Object[]{tariffType},
                    Long.class
            );
            return Optional.ofNullable(id);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }


    private Tariff mapRowToTariff(ResultSet row, int rowNum)
            throws SQLException {
        return new Tariff(row.getLong("id"), row.getString("type"),
                row.getString("interest_rate"));
    }
}
