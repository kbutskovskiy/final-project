package com.mtsb.finalproject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mtsb.finalproject.dto.OrderDeleteRestDTO;
import com.mtsb.finalproject.rest_controller.OrderDeleteControllerRest;
import com.mtsb.finalproject.service.DeleteService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import com.mtsb.finalproject.exception.OrderImpossibleToDeleteException;

import java.util.UUID;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(OrderDeleteControllerRest.class)
@AutoConfigureMockMvc(addFilters = false)
public class DeleteControllerRestTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private DeleteService deleteService;

    @Test
    public void deleteOrder_success() throws Exception {
        OrderDeleteRestDTO orderDTO = new OrderDeleteRestDTO();
        orderDTO.setUserId(120356894755L);
        orderDTO.setOrderId(UUID.fromString("37f38d66-6c55-43b5-b981-0f9fd0b448d7"));

        doNothing().when(deleteService).deleteFromLoan(orderDTO.getUserId(), String.valueOf(orderDTO.getOrderId()));

        mockMvc.perform(delete("/loan-service/deleteOrder")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(orderDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteOrder_failure() throws Exception {
        OrderDeleteRestDTO orderDTO = new OrderDeleteRestDTO();
        orderDTO.setUserId(120356894755L);
        orderDTO.setOrderId(UUID.fromString("37f38d66-6c55-43b5-b981-0f9fd0b448d7"));

        doThrow(new OrderImpossibleToDeleteException("Невозможно удалить заявку"))
                .when(deleteService).deleteFromLoan(orderDTO.getUserId(), String.valueOf(orderDTO.getOrderId()));

        mockMvc.perform(delete("/loan-service/deleteOrder")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(orderDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error.code").value("ORDER_IMPOSSIBLE_TO_DELETE"))
                .andExpect(jsonPath("$.error.message").value("Невозможно удалить заявку"));
    }
}
