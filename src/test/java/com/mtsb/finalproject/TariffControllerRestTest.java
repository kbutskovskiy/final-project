package com.mtsb.finalproject;

import com.mtsb.finalproject.entity.Tariff;
import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseTariffs;
import com.mtsb.finalproject.security.SecurityConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.mtsb.finalproject.service.TariffService;
import com.mtsb.finalproject.rest_controller.TariffControllerRest;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(SecurityConfig.class)
@WebMvcTest(controllers = TariffControllerRest.class)
public class TariffControllerRestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TariffService tariffService;

    @MockBean
    private DataSource dataSource;

    @Test
    public void getTariffsTest() throws Exception {
        List<Tariff> tariffs = Arrays.asList(
                new Tariff(1L, "CONSUMER", "11.9%"),
                new Tariff(2L, "MORTGAGE", "5.9%")
        );

        when(tariffService.getTariffs()).thenReturn(new Response<>(new SuccessResponseTariffs(tariffs)));

        mockMvc.perform(get("/loan-service/getTariffs")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..tariffs[0].id").value(1))
                .andExpect(jsonPath("$..tariffs[0].type").value("CONSUMER"))
                .andExpect(jsonPath("$..tariffs[0].interest_rate").value("11.9%"))
                .andExpect(jsonPath("$..tariffs[1].id").value(2))
                .andExpect(jsonPath("$..tariffs[1].type").value("MORTGAGE"))
                .andExpect(jsonPath("$..tariffs[1].interest_rate").value("5.9%"));
    }
}