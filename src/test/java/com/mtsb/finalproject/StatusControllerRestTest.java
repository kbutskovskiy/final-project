package com.mtsb.finalproject;

import com.mtsb.finalproject.exception.OrderNotFoundException;
import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseStatus;
import com.mtsb.finalproject.rest_controller.StatusControllerRest;
import com.mtsb.finalproject.service.StatusService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StatusControllerRest.class)
public class StatusControllerRestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StatusService statusService;

    @Test
    @WithMockUser(username = "kbutskovskiy", password = "$2a$12$ijUzIKj4CQ5JNSmgeSeS1uDwomQaLMETJ5SzX4MUr/6qOnVcNmBDK")
    void getStatusOrderSuccess() throws Exception {
        UUID orderId = UUID.fromString("37f38d66-6c55-43b5-b981-0f9fd0b448d7");

        ApiResponse expectedResponse = new Response<>(new SuccessResponseStatus("IN_PROGRESS"));
        when(statusService.getStatus(orderId)).thenReturn(expectedResponse);

        mockMvc.perform(get("/loan-service/getStatusOrder")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("orderId", orderId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.orderStatus").value("IN_PROGRESS"));
    }

    @Test
    @WithMockUser(username = "kbutskovskiy", password = "$2a$12$ijUzIKj4CQ5JNSmgeSeS1uDwomQaLMETJ5SzX4MUr/6qOnVcNmBDK")
    void getStatusOrderFailure() throws Exception {
        UUID orderId = UUID.fromString("37f38d66-6c55-43b5-b981-0f9fd0b448d7");

        when(statusService.getStatus(orderId)).thenThrow(new OrderNotFoundException("Заявка не найдена"));

        mockMvc.perform(get("/loan-service/getStatusOrder")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("orderId", orderId.toString()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error.code").value("ORDER_NOT_FOUND"))
                .andExpect(jsonPath("$.error.message").value("Заявка не найдена"));
    }

}
