package com.mtsb.finalproject;

import com.mtsb.finalproject.constant.ErrorCodes;
import com.mtsb.finalproject.exception.CustomException;
import com.mtsb.finalproject.response.ApiResponse;
import com.mtsb.finalproject.response.Response;
import com.mtsb.finalproject.response.SuccessResponseOrder;
import com.mtsb.finalproject.rest_controller.OrderControllerRest;
import com.mtsb.finalproject.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.security.test.context.support.WithMockUser;


import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(OrderControllerRest.class)
public class OrderControllerRestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @Test
    @WithMockUser(username = "kbutskovskiy", password = "$2a$12$ijUzIKj4CQ5JNSmgeSeS1uDwomQaLMETJ5SzX4MUr/6qOnVcNmBDK")
    public void testGetOrder_Success() throws Exception {
        Long userId = 120356894755L;
        Long tariffId = 1L;
        String orderId = "37f38d66-6c55-43b5-b981-0f9fd0b448d7";
        ApiResponse apiResponse = new Response<>(new SuccessResponseOrder(orderId));

        when(orderService.tariffRestProcessing(userId, tariffId)).thenReturn(apiResponse);

        mockMvc.perform(post("/loan-service/order")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":120356894755,\"tariffId\":1}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.order_id").value(orderId));
    }

    @Test
    @WithMockUser(username = "kbutskovskiy", password = "$2a$12$ijUzIKj4CQ5JNSmgeSeS1uDwomQaLMETJ5SzX4MUr/6qOnVcNmBDK")
    public void testGetOrder_CustomException() throws Exception {
        Long userId = 120356894755L;
        Long tariffId = 1L;

        when(orderService.tariffRestProcessing(userId, tariffId))
                .thenThrow(new CustomException(ErrorCodes.TARIFF_NOT_FOUND, "Тариф не найден"));

        mockMvc.perform(post("/loan-service/order")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"userId\":120356894755,\"tariffId\":1}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error.code").value("TARIFF_NOT_FOUND"))
                .andExpect(jsonPath("$.error.message").value("Тариф не найден"));
    }

}
